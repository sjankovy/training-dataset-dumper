#ifndef STREAMERS_HH
#define STREAMERS_HH

#include "xAODJet/Jet.h"

std::ostream& operator<<(std::ostream& str, const xAOD::Jet& jet) {
  str << "pt: " << jet.pt() << ", eta: " << jet.eta() << ", phi: " << jet.phi()
      << ", numConstituents: " << jet.numConstituents();
  return str;
}

#endif
