#ifndef TRACKSORT_H
#define TRACKSORT_H

#include "TrackSortOrder.hh"

#include "xAODTracking/TrackParticleFwd.h"
#include "xAODJet/JetFwd.h"

#include <vector>
#include <functional>
#include <string>

using TPV = std::vector<const xAOD::TrackParticle*>;

using TrackSort = std::function<TPV(const TPV&, const xAOD::Jet&)>;

TrackSort trackSort(TrackSortOrder order, const std::string prefix);

#endif
